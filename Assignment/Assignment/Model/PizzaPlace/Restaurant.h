//
//  Restaurant.h
//  
//
//  Created by orlangur on 7/14/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Restaurant : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSString * phone;

@end
