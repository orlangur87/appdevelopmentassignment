//
//  Restaurant.m
//  
//
//  Created by orlangur on 7/14/15.
//
//

#import "Restaurant.h"


@implementation Restaurant

@dynamic name;
@dynamic desc;
@dynamic rating;
@dynamic phone;

@end
