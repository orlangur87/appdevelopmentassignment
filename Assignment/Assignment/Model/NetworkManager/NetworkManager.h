//
//  NetworkManager.h
//  Assignment
//
//  Created by orlangur on 7/14/15.
//  Copyright (c) 2015 orlangur. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UXRFourSquareNetworkingEngine;

typedef void(^CallbackBlock)(NSArray *array, NSError *error);

@interface NetworkManager : NSObject

@property (strong) UXRFourSquareNetworkingEngine *fourSquareEngine;

+ (NetworkManager *)sharedInstance;
- (void)startFoursquare;
- (void)updateRestaurantsList:(CallbackBlock)callback;
- (NSArray *)extractRestaurants;

@end
