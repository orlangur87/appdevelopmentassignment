//
//  NetworkManager.m
//  Assignment
//
//  Created by orlangur on 7/14/15.
//  Copyright (c) 2015 orlangur. All rights reserved.
//

#import "NetworkManager.h"
#import "LocationManager.h"
#import "FourSquareKit.h"
#import "AppDelegate.h"
#import "Restaurant.h"

NSString *const yourClientId = @"DANQPADNGFTIRVDH3O3XULNLPBKXS1JEDQFIDMTV4X0TC352";
NSString *const yourClientSecret = @"KBPSY4BNKHWAB2ZRR4RG04UXJCWVFCOJZEKRQWQNWFHPE4E3";
NSString *const yourCallbackURl = @"google.com";
NSString *const kRestaurantsNotificationName = @"restaurantsUpdated";
NSString *const kCurrentLocationName = @"currentLocation";
NSString *const kRestaurantEntityName = @"PizzaPlace";

@implementation NetworkManager

+ (NetworkManager *)sharedInstance {
    static dispatch_once_t pred;
    static NetworkManager *sharedInstance = nil;
    
    dispatch_once(&pred, ^{ sharedInstance = [[self alloc] init]; });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [[LocationManager sharedInstance] addObserver:self forKeyPath:kCurrentLocationName options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc {
    [[LocationManager sharedInstance] removeObserver:self forKeyPath:kCurrentLocationName];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [self updateRestaurantsList:^(NSArray *array, NSError *error) {
    }];
}

- (void)startFoursquare {
    [UXRFourSquareNetworkingEngine registerFourSquareEngineWithClientId:yourClientId andSecret:yourClientSecret andCallBackURL:yourCallbackURl];
    self.fourSquareEngine = [UXRFourSquareNetworkingEngine sharedInstance];
}

- (void)updateRestaurantsList:(CallbackBlock)callback {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self getRestaurantsForCurrentLocation:^(NSArray *array, NSError *error) {
            if (array) {
                AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
                context.persistentStoreCoordinator = [appDelegate persistentStoreCoordinator];
                
                for (UXRFourSquareRestaurantModel *restaurant in array) {
                    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kRestaurantEntityName];
                    request.predicate = [NSPredicate predicateWithFormat:@"name == %@", restaurant.name];
                    NSArray *ar = [context executeFetchRequest:request error:nil];
                    if (ar.count == 0) {
                        NSEntityDescription *entity = [NSEntityDescription entityForName:kRestaurantEntityName inManagedObjectContext:context];
                        Restaurant *r = [[Restaurant alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
                        r.name = restaurant.name;
                        r.desc = restaurant.description;
                        r.rating = restaurant.rating;
                        r.phone = restaurant.contact.phone;
                    }
                }
                
                NSError *er;
                [context  save:&er];
                if (er) {
                    NSLog(@"Failed to save restaurants: %@", er);
                }
              
                [[NSNotificationCenter defaultCenter] postNotificationName:kRestaurantsNotificationName object:nil userInfo:nil];
            }
            
            callback(array, error);
        }];
    });
}

- (NSArray *)extractRestaurants {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kRestaurantEntityName];
    request.fetchLimit = 5;
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    NSError *er;
    NSArray *ar = [appDelegate.managedObjectContext executeFetchRequest:request error:&er];
    if (er) {
        NSLog(@"Failed to extract restaurants: %@", er);
        return nil;
    } else {
        return ar;
    }
}

- (void)getRestaurantsForCurrentLocation:(CallbackBlock)callback {
    CLLocation *currentLocation = [[LocationManager sharedInstance] currentLocation];
    
    if (currentLocation) {
        [self.fourSquareEngine exploreRestaurantsNearLatLong:currentLocation.coordinate withQuery:@"pizza" withCompletionBlock:^(NSArray *restaurants) {
            callback(restaurants, nil);
        } failureBlock:^(NSError *error) {
            callback(nil, error);
        }];
    } else {
        NSError *error = [NSError errorWithDomain:@"Location error domain" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Failed to receive your current location"}];
        
        callback(nil, error);
    }
    
}

@end
