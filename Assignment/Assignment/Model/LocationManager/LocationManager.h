//
//  LocationManager.h
//  Assignment
//
//  Created by orlangur on 7/14/15.
//  Copyright (c) 2015 orlangur. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@interface LocationManager : NSObject  <CLLocationManagerDelegate>

@property (strong) CLLocation *currentLocation;

+ (LocationManager *)sharedInstance;
- (void)startService;

@end
