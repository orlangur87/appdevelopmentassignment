//
//  LocationManager.m
//  Assignment
//
//  Created by orlangur on 7/14/15.
//  Copyright (c) 2015 orlangur. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager {
    CLLocationManager *locationManager;
}

+ (LocationManager *)sharedInstance {
    static dispatch_once_t pred;
    static LocationManager *sharedInstance = nil;
    
    dispatch_once(&pred, ^{ sharedInstance = [[self alloc] init]; });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        locationManager = [CLLocationManager new];
        locationManager.delegate = self;
    }
    return self;
}

- (void)startService {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [locationManager startUpdatingLocation];
    } else {
        [locationManager requestWhenInUseAuthorization];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.currentLocation = locationManager.location;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [locationManager startUpdatingLocation];
    }
}

@end
