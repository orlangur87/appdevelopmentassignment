//
//  DetailsVC.h
//  Assignment
//
//  Created by orlangur on 7/14/15.
//  Copyright (c) 2015 orlangur. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Restaurant;

@interface DetailsVC : UIViewController

@property (strong, nonatomic) Restaurant *restaurant;

@end
