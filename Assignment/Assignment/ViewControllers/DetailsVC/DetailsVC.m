//
//  DetailsVC.m
//  Assignment
//
//  Created by orlangur on 7/14/15.
//  Copyright (c) 2015 orlangur. All rights reserved.
//

#import "DetailsVC.h"
#import "Restaurant.h"

@interface DetailsVC () {
    __weak IBOutlet UILabel *nameLabel;
}

@end

@implementation DetailsVC

- (void)viewDidLoad  {
    [super viewDidLoad];
    
    nameLabel.text = [NSString stringWithFormat:@"Name: %@\nDescription: %@\nRating: %@\nPhone: %@", _restaurant.name, _restaurant.desc, _restaurant.rating, _restaurant.phone];
}
@end
