//
//  RestaurantsListVC.m
//  Assignment
//
//  Created by orlangur on 7/14/15.
//  Copyright (c) 2015 orlangur. All rights reserved.
//

#import "RestaurantsListVC.h"
#import "LocationManager.h"
#import "RestaurantCell.h"
#import "Restaurant.h"
#import "AppDelegate.h"
#import "NetworkManager.h"
#import "DetailsVC.h"

NSString *const cellID = @"cellID";
extern NSString *const kRestaurantsNotificationName;

@interface RestaurantsListVC () <UITableViewDelegate, UITableViewDataSource> {
    __weak IBOutlet UITableView *tableView;
    NSArray *restaurants;
}

@end

@implementation RestaurantsListVC

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        restaurants = @[];
        NSArray *ar = [[NetworkManager sharedInstance] extractRestaurants];
        if (ar) {
            restaurants = ar;
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observationAction) name:kRestaurantsNotificationName object:nil];
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [tableView registerNib:[UINib nibWithNibName:@"RestaurantCell" bundle:nil] forCellReuseIdentifier:cellID];
    
    [[NetworkManager sharedInstance] addObserver:self forKeyPath:kRestaurantsNotificationName options:NSKeyValueObservingOptionNew context:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (tableView.indexPathForSelectedRow) {
        [tableView deselectRowAtIndexPath:tableView.indexPathForSelectedRow animated:YES];
    }
}

- (void)observationAction {
    NSArray *ar = [[NetworkManager sharedInstance] extractRestaurants];
    
    if (ar) {
        restaurants = ar;
        [tableView reloadData];
    }
}

- (void)getRestaurantsList {
    [[NetworkManager sharedInstance] updateRestaurantsList:^(NSArray *array, NSError *error) {
        if (array) {
            
        } else if (error) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Undefined error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

#pragma mark - UITableView Datasource/Delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger count = restaurants.count;
    return count > 5 ? 5 : count;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RestaurantCell *cell = (RestaurantCell *)[_tableView dequeueReusableCellWithIdentifier:cellID];
    
    Restaurant *restaurant = restaurants[indexPath.row];
    
    cell.nameLabel.text = restaurant.name;
    
    return cell;
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"details" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DetailsVC *details = segue.destinationViewController;
    details.restaurant = restaurants[tableView.indexPathForSelectedRow.row];
}


@end
